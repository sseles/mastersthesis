﻿using GenericController.Models;
using GenericController.Models.Database;
using GenericController.Models.Database.Base;
using GenericController.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace GenericController.Controllers.Base
{
    public class BaseController<T> : Controller
        where T : BaseEntity
    {
        protected ApplicationDbContext _db { get; set; }
        protected BaseRepository<T> _repository { get; set; }

        public BaseController()
        {
            _db = new ApplicationDbContext();
            _repository = new BaseRepository<T>(_db);
        }

        [HttpGet]
        public ActionResult Index(int elements = 30, int page = 1, bool order = true, string entityName = null, int entityId = 0)
        {
            if (elements < 1) elements = 30;
            if (page < 1) page = 1;

            IQueryable<T> entries = _repository.All;
            ViewBag.TotalEntries = entries.Count();

            List<T> result = GetFilteredItems(elements, page, order, entityName, entityId);

            SetTitleName();

            return View("SharedIndex", result);
        }

        [HttpGet]
        public virtual ActionResult Create()
        {
            SetDropdowns();
            return View("SharedCreate", Activator.CreateInstance(typeof(T)));
        }

        [HttpPost]
        public ActionResult Create(T entity)
        {
            if (ModelState.IsValid)
            {
                _repository.Add(entity);
                _repository.SaveChanges();

                return RedirectToAction("Index");
            }

            SetDropdowns();
            return View("SharedCreate", entity);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            SetDropdowns();
            return View("SharedEdit", _repository.Get(id));
        }

        [HttpPost]
        public ActionResult Edit(T entity)
        {
            if (ModelState.IsValid)
            {
                _repository.Edit(entity);
                _repository.SaveChanges();

                return RedirectToAction("Index");
            }

            SetDropdowns();
            return View("SharedEdit", entity);
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            return View("SharedDetails", _repository.Get(id, SetIncludes()));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            try
            {
                _repository.Delete(id);
                _repository.SaveChanges();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex);

                return RedirectToAction("Details", id);
            }

            return RedirectToAction("Index");
        }


        protected void SetDropdowns()
        {
            var dictionary = new Dictionary<string, List<SelectListItem>>();
            foreach (PropertyInfo pi in typeof(T).GetProperties())
            {
                if (pi.Name == "Id") continue;
                else if (pi.Name.Contains("Id") == false) continue;
                else
                {
                    string name = pi.Name.Remove(pi.Name.LastIndexOf("Id"), 2);
                    var c = ClassParser.Parse(name);
                    var list = new List<SelectListItem>();
                    foreach (BaseEntity entry in _db.Set(c).AsQueryable())
                    {
                        list.Add(new SelectListItem { Value = entry.Id.ToString(), Text = entry.Name });
                    }
                    dictionary.Add(pi.Name, list);
                }
            }

            ViewBag.Dropdown = dictionary;
        }

        protected IEnumerable<string> SetIncludes()
        {
            List<string> list = new List<string>();

            foreach (PropertyInfo pi in typeof(T).GetProperties())
            {
                if (pi.Name.Contains("Id") == true && pi.Name.Length > 2)
                {
                    string name = pi.Name.Remove(pi.Name.LastIndexOf("Id"), 2);
                    list.Add(name);
                }
            }

            return list;
        }

        protected void SetTitleName()
        {
            ViewBag.TitleName = typeof(T).Name;
        }

        protected List<T> GetFilteredItems(int elements, int page, bool order, string entityName, int entityId)
        {
            IQueryable<T> items = _repository.All;

            if (string.IsNullOrWhiteSpace(entityName) == false && entityId > 0)
            {
                ParameterExpression parameter = Expression.Parameter(typeof(T));

                var condition =
                        Expression.Lambda<Func<T, bool>>(
                            Expression.Equal(
                                Expression.Property(parameter, entityName),
                                Expression.Constant(entityId)
                            ),
                            parameter
                        );

                items = items.Where(condition);
            }
            if (order)
            {
                items = items.OrderBy(i => i.Id);
            }
            else
            {
                items = items.OrderByDescending(i => i.Id);
            }

            items = items.Skip(elements * (page - 1)).Take(elements);

            return items.ToList();
        }
    }
}
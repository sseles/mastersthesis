﻿using GenericController.Controllers.Base;
using GenericController.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GenericController.Controllers
{
    public class AddressController : BaseController<Address>
    {
        [HttpGet]
        public override ActionResult Create()
        {
            DoSomething();
            return base.Create();
        }

        protected void DoSomething()
        {
            //Your code here
        }
    }
}
namespace GenericController.Migrations
{
    using Models.Database;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<GenericController.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(GenericController.Models.ApplicationDbContext context)
        {
            context.Cities.AddOrUpdate(c => c.Id,
                new City { Id = 1, Name = "Zagreb" },
                new City { Id = 2, Name = "Split" },
                new City { Id = 3, Name = "Rijeka" });

            context.Addresses.AddOrUpdate(a => a.Id,
                new Address { Id = 1, Name = "Ilica", CityId = 1 },
                new Address { Id = 2, Name = "Vukovarska", CityId = 1 },
                new Address { Id = 3, Name = "Moslavacka", CityId = 1 },
                new Address { Id = 4, Name = "Rotor", CityId = 1 },
                new Address { Id = 5, Name = "Traktorska", CityId = 1 },
                new Address { Id = 6, Name = "Napusena", CityId = 2 },
                new Address { Id = 7, Name = "Vesela", CityId = 2 },
                new Address { Id = 8, Name = "Mokra", CityId = 2 }
                );

            context.Residents.AddOrUpdate(r => r.Id,
                new Resident { Id = 1, Name = "Mirko", AddressId = 1, Phone = 987654321 },
                new Resident { Id = 2, Name = "Marko", AddressId = 2, Phone = 987654321 },
                new Resident { Id = 3, Name = "Mario", AddressId = 3, Phone = 987654321 },
                new Resident { Id = 4, Name = "Marijan", AddressId = 4, Phone = 987654321 },
                new Resident { Id = 5, Name = "Marija", AddressId = 5, Phone = 987654321 },
                new Resident { Id = 6, Name = "Mirta", AddressId = 1, Phone = 987654321 },
                new Resident { Id = 7, Name = "Mirna", AddressId = 2, Phone = 987654321 },
                new Resident { Id = 8, Name = "Mlaka", AddressId = 3, Phone = 987654321 },
                new Resident { Id = 9, Name = "Mokri", AddressId = 4, Phone = 987654321 },
                new Resident { Id = 10, Name = "Medvjed", AddressId = 3, Phone = 987654321 },
                new Resident { Id = 11, Name = "Meded", AddressId = 3, Phone = 987654321 }
                );
        }
    }
}

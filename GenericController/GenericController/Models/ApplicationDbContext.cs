﻿using GenericController.Models.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GenericController.Models
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Resident> Residents { get; set; }

        public DbSet<City> Cities { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<Data> Datas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Data>()
            //.HasOptional<Resident>(s => s.Resident)
            //.WithMany()
            //.WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }
    }
}
﻿using GenericController.Models.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Web;

namespace GenericController.Models.Repository
{
    public class BaseRepository<Entity> where Entity : BaseEntity
    {
        ApplicationDbContext _db;

        public BaseRepository()
        {
            _db = new ApplicationDbContext();
        }

        public BaseRepository(ApplicationDbContext context)
        {
            _db = context;
        }

        public IQueryable<Entity> All
        {
            get
            {
                return _db.Set<Entity>().AsQueryable();
            }
        }

        public Entity Get(int id)
        {
            return _db.Set<Entity>().Find(id);
        }

        public Entity Get(int id, IEnumerable<string> includes)
        {
            DbQuery<Entity> entitys = _db.Set<Entity>();

            foreach (string include in includes)
            {
                entitys = entitys.Include(include);
            }

            return entitys.FirstOrDefault(e => e.Id == id);
        }

        public void Add(Entity entity)
        {
            entity.CreatedDate = DateTime.Now;
            _db.Set<Entity>().Add(entity);
        }

        public void Edit(Entity entity)
        {
            Entity oldEntity = Get(entity.Id);
 
            foreach (PropertyInfo pi in entity.GetType().GetProperties())
            {
                if (pi.Name == "Id") continue;

                pi.SetValue(oldEntity, pi.GetValue(entity));
            }

            oldEntity.UpdatedDate = DateTime.Now;

            _db.Entry(oldEntity).State = System.Data.Entity.EntityState.Modified;
        }

        public void Delete(int id)
        {
            Entity entity = Get(id);
            _db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }
    }
}
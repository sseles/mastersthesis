﻿using GenericController.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GenericController.Models.Database
{
    public class Resident : BaseEntity
    {
        [Required]
        [ForeignKey("Address")]
        public int AddressId { get; set; }

        public int Phone { get; set; }

        [Hidden]
        public Address Address { get; set; }

        [Hidden]
        public ICollection<Data> Data { get; set; }
    }
}
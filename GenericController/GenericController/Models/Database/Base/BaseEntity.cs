﻿using GenericController.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GenericController.Models.Database
{
    public abstract class BaseEntity
    {
        [Key]
        [Required]
        [ReadOnly]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [ReadOnly]
        [Column(TypeName = "DateTime2")]
        public DateTime CreatedDate { get; set; }

        [ReadOnly]
        [Column(TypeName = "DateTime2")]
        public DateTime UpdatedDate { get; set; }
    }
}
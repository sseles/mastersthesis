﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenericController.Models.Database.Base
{
    public static class ClassParser
    {
        public static Type Parse(string name)
        {
            switch (name)
            {
                case "Address": return typeof(Address);
                case "City": return typeof(City);
                case "Resident": return typeof(Resident);
                case "Data": return typeof(Data);
                default: return null;
            }
        }
    }
}
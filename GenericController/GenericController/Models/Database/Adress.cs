﻿using GenericController.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GenericController.Models.Database
{
    public class Address : BaseEntity
    {
        [Required]
        [ForeignKey("City")]
        public int CityId { get; set; }

        [Hidden]
        public City City { get; set; }

        [Hidden]
        public ICollection<Resident> Residents { get; set; }
    }
}
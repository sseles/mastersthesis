﻿using GenericController.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GenericController.Models.Database
{
    public class City : BaseEntity
    {
        [Hidden]
        public ICollection<Address> Addresses { get; set; }
    }
}